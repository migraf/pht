from aiohttp import web
import socketio
from pht import Station


sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)

station = Station(config="station_config.yml")

@sio.event
def connect(sid, environ):
    print("connect ", sid)

@sio.event
async def chat_message(sid, data):
    print("message ", data)
    await sio.emit('reply', room=sid)

@sio.event
def disconnect(sid):
    print('disconnect ', sid)


if __name__ == '__main__':
    web.run_app(app, host=station.address, port=station.port)


