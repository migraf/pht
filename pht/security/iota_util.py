from iota import Iota
import random
import string


def generate_seed():
    seed = "".join(random.choices(string.ascii_uppercase + "9", k=81))
    return seed


if __name__ == '__main__':
    print(generate_seed())
    print(generate_seed())
