import os
from pht.db.database_models import Train as db_train
from pht.analysis.models import DistributedModel
from pht.data.DataProvider import DataProvider
from .TrainState import TrainState


class Train:
    """
    Class representing a train object consisting of a distributed model
    """

    def __init__(self, model: DistributedModel = None, data_provider: DataProvider = None, state: TrainState = None):
        self.model = model
        self.data_provider = data_provider
        self.state = state
        # TODO validate

    def execute_train(self):
        if self.state.finished_discovery:
            X, y = self.data_provider.provide()
            self.model.fit(X, y)
            self.model.distribute()
        else:
            self.data_provider.discover()

    def submit_train(self):
        """
        Create a train submission generating a folder containing all necessary configs and potentially files

        Returns:

        """
