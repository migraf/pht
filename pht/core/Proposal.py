from datetime import datetime


class Proposal:
    """
    Class representing a train proposal, can perform submit for newly created proposal or validate based on an external
    instance
    """
    def __init__(self, name=None, description=None, submitter=None, target=None, variables=None, model=None):
        self.model = model
        self.variables = variables
        self.target = target
        self.submitter = submitter
        self.description = description
        self.name = name


