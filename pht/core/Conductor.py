import socketio
from pymongo import MongoClient
import json
import os
import datetime

from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split

import pandas as pd
from sklearn.preprocessing import StandardScaler
import pickle
import numpy as np


class Conductor:
    def __init__(self, train_config, mongo_db=None, data_source=None, model_path=None):
        """
        Creates a Conductor object associated with a specific train, that manages its execution and distribution
        :param train_config: path to json object or dictionary with train configuration
        :type train_config:
        :param mongo_db: url to the mongo db instance
        :type mongo_db:
        """
        self.station = "leipzig"
        self.train_config = train_config
        self.train_id = None
        if mongo_db:
            self.mongo_client = self.init_mongodb_client_(mongo_db)
        else:
            self.mongo_client = self.init_mongodb_client_("localhost:27017")
        self.train = None
        self.initialize_train(self.train_config)
        if data_source:
            self.data = load_data(data_source)
        self.model = None
        self.trained_model = None

        if model_path:
            self.model_path = model_path
        else:
            self.model_path = os.path.join(os.getcwd(), f"trained_models_{self.train_id}")

    def examine_train(self, ):
        pass

    def provide_model(self, train):
        pass

    def run_train(self, model_state):
        """
        Execute the model contained in the train
        :return:
        :rtype:
        """
        # Distribution Mode 1 equivalent exchange
        db = self.mongo_client.train_db
        if self.train_config["type"]["distribution_type"] == "catboost":
            print(len(self.data))
            if not self.model:
                self.create_model_(self.data)
            self.model.set_config(self.train["model"])
            self.model.fit()
            self.model.save()
            # Store results in data base
            with open(self.model_path, "r", encoding="utf8") as model_json:
                trained_model = model_json.read()
            # Store results depending on exchange type
            self.trained_model = trained_model
            db.trains.update_one({"_id": self.train_id}, {"$set": {"trained_models": {self.station: trained_model}}})

        """
        Online Training Mode Including weight and variable discovery mode
        """
        if self.train_config["type"]["distribution_type"] == "online":

            # TODO handle this differently
            # When all stations have participated in discovery run the train
            if len(self.train["discovery_state"]["visited_discovery"]) == len(self.train["stations"]):
                if self.train["trained_model"]:
                    # TODO Load model from ipfs file system based on config
                    # TODO load model_state
                    self.model = pickle.loads(self.train["trained_model"])
                else:
                    # Create necessary model files based on config
                    self.create_model_()
                self.train_model()

            # If discovery is not finished perform data discovery and update train state
            else:
                self.discover()


    def train_model(self):
        # TODO read training config and fit model with data based on this
        pass

    def load_model(self):
        pass


    def discover(self):
        """
        Perform data and class discovery and update the discovery state of the train
        :return:
        :rtype:
        """
        db = self.mongo_client.train_db
        print("Running Data Discovery")

        online_config = self.train_config["type"]["distribution_config"]

        self.train["discovery_state"]["visited_discovery"].append(self.station)
        if self.train["discovery_state"]["total_samples"]:
            # Update number of total samples
            self.train["discovery_state"]["total_samples"] += len(self.data[self.train["target"]]
                                                                  [self.data[self.train["target"]].notnull()])
        else:
            self.train["discovery_state"]["total_samples"] = len(self.data[self.train["target"]]
                                                                 [self.data[self.train["target"]].notnull()])

        # Find all categories of the categorical variable available in the data and store them
        if online_config["cat_var_discovery"]:
            cat_var_types = self.discover_categories()
            self.train["discovery_state"]["cat_var_types"] = cat_var_types
        # Add the number of samples per target class to the train state
        classes = self.discover_class_distribution()
        # TODO only do this in multi class classification case
        for cl, count in classes.items():
            if self.train["discovery_state"]["target_class_counts"].get(str(cl), None):
                self.train["discovery_state"]["target_class_counts"][str(cl)] += count
            else:
                self.train["discovery_state"]["target_class_counts"][str(cl)] = count

        num_data = self.data[self.train["variables"]["continuous"]].copy()
        if self.train["discovery_state"]["num_var_scaler"]:
            scaler = pickle.loads(self.train["discovery_state"]["num_var_scaler"])
            scaler.partial_fit(num_data)
        else:
            scaler = StandardScaler()
            scaler.partial_fit(num_data)
        s = pickle.dumps(scaler)
        self.train["discovery_state"]["num_var_scaler"] = s

        # update train state in DB
        db.trains.update_one({"_id": self.train_id},
                             {"$set": {"discovery_state": self.train["discovery_state"]}})


    def discover_categories(self):
        """
        Extract the possible categories for the selected categorical variables from the set data source
        :return:
        :rtype:
        """
        cat_var_types = self.train["train_state"]["cat_var_types"]
        for var in self.train["variables"]["categorical"]:
            categories = cat_var_types.get(var, None)
            if categories:
                # Union between all existing categories for all set categorical variables
                cat_new = set(list(self.data[var].unique())).union(set(categories))
                cat_var_types[var] = list(cat_new)
            else:
                cat_var_types[var] = list(self.data[var].unique())
        return cat_var_types

    def discover_class_distribution(self):
        """
        Calculate the class distribution for calculating class weights for the train depending on station data
        :return:
        :rtype:
        """
        classes = self.data[self.train["target"]][self.data[self.train["target"]].notnull()].value_counts().to_dict()
        print(classes)
        return classes

    def initialize_train(self, train_config):
        """
        Creates a new train document and persists it in the database
        :return:
        :rtype:
        """
        if type(train_config) == dict:
            self.train = train_config
        else:
            with open(train_config, "r", encoding="utf8") as tc_json:
                self.train = json.loads(tc_json.read())
        if not self.train.get("created", None):
            self.train["created"] = datetime.datetime.now()
        self.train["modified"] = datetime.datetime.now()
        db = self.mongo_client.train_db
        train = db.trains.find_one({"name": self.train_config["name"]})
        if train:
            self.train = train
            db.trains.update_one({"name": self.train_config["name"]},
                                 {"$set": {"modified": datetime.datetime.now()}})
            self.train_id = train["_id"]
        else:
            self.save_train_()

    def create_training_data(self, validation_set=False):
        """
        Creates training data for executing a train based on the a finished discovery train
        :return:
        :rtype:
        """
        # data = self.data.dropna(how="all", axis=1)
        dummy_df = self.encode_categorical_variables_()
        # Standardize numerical variables
        num_df = self.data[self.train["variables"]["continuous"]].copy()
        target = self.data[self.train["target"]]

        # Impute missing values
        imp = SimpleImputer(missing_values=np.nan, strategy="median")
        num_df = imp.fit_transform(num_df)
        # Scale numerical variables to zero mean and unit variance with the distributed scaler
        scaler = pickle.loads(self.train["train_state"]["num_var_scaler"])
        num_df = scaler.transform(num_df)
        num_df = pd.DataFrame(columns=self.train["variables"]["continuous"], data=num_df)
        dummy_df = dummy_df.reset_index()
        data = pd.concat([num_df, dummy_df], axis=1, ignore_index=True)
        data = data.dropna(how="all")
        # TODO double check indexing
        target.index = data.index
        data = data[target.notnull()]
        target = target[target.notnull()]

        print("data shape: ", data.shape)
        print("len target: ", len(target))
        target = target.astype(int)
        for col in data.columns:
            data[col] = data[col].astype("float")
        data = data.fillna(method="pad", axis="columns")
        if validation_set:
            x_train, x_val, y_train, y_val = train_test_split(data, target)
            return x_train, x_val, y_train, y_val
        else:
            return data, target

    def encode_categorical_variables_(self):
        """
        Onehot encode categorical variables using the overall categories found during the discovery phase
        :return:
        :rtype:
        """
        cat_df = self.data[self.train["variables"]["categorical"]].copy()
        dummy_df = pd.DataFrame()
        for var, categories in self.train["train_state"]["cat_var_types"].items():
            unique_cats = np.unique(categories)
            cat_df[var] = cat_df[var].astype("category")
            cat_df[var].cat.set_categories(unique_cats, inplace=True)
            dummies = pd.get_dummies(cat_df[var], prefix=var, dummy_na=True, dtype=float)
            dummy_df = pd.concat([dummy_df, dummies], axis=1)

        print(dummy_df.info())
        return dummy_df

    async def share_results(self):
        """
        Send the trained model to the station for
        :return:
        :rtype:
        """
        sio = socketio.AsyncClient()
        participants = self.train["stations"]

        sio = socketio.AsyncClient()

        # await sio.connect(station["address"])
        print(participants[1]["address"])
        await sio.connect("http://localhost:11111")
        print('my sid is', sio.sid)

        train_results = {
            "station": self.station,
            "completed": datetime.datetime.now().__str__(),
            "model": self.trained_model,
            "train_id": str(self.train_id),
            "num_samples": len(self.data)
        }

        @sio.event
        def disconnect():
            print('disconnected from server')

        await sio.emit(event="train_finished", data={"finished": train_results})
        # await sio.emit(event="test_event", data={"train_result": self.trained_model})
        await sio.wait()

    def assemble_train(self, train_config, results):
        """
        Assemble a train based on the results of distributed analysis
        :param train_config: train configuration json
        :type train_config:
        :param results: list of results from other stations
        :type results:
        :return:
        :rtype:
        """
        result_models = [{"model": result["model"], "station": result["station"]} for result in results]
        self.create_model_(data=None)
        self.model.assemble(result_models)

    def save_train_(self):
        """
        Stores the current train instance in a mongo db document
        :return:
        :rtype:
        """
        # TODO check if it already exists and then update instead
        db = self.mongo_client.train_db
        db.trains.insert_one(self.train)
        db_train_id = db.trains.find_one(self.train)["_id"]
        self.train_id = db_train_id

    def load_train(self):
        db = self.mongo_client.train_db
        train = db.trains.find_one(self.train)
        print(train)
        return train

    @staticmethod
    def init_mongodb_client_(connection):
        client = MongoClient(connection)
        return client

    def get_data_(self, path):
        """
        Get the data associated with the algorithm
        :return:
        :rtype:
        """
        # TODO make this more generic
        df_sars = load_data(
            path,
            two_sheets=False)
        return df_sars

    def set_data(self, data):
        self.data = data

    def create_model_(self, data=None):
        model_config = self.train["model"]
        print(model_config)

        cat_vars = self.train["variables"]["categorical"]
        num_vars = self.train["variables"]["continuous"]
        target = self.train["target"]
        if model_config["type"] == "catboost":
            variables = num_vars + cat_vars
            model = CatboostDistributedModel(name=self.train["name"],
                                             variables=variables,
                                             target=target,
                                             data=data,
                                             model_path=self.model_path)
            self.model = model
        elif model_config["type"] == "SGD":
            # TODO add
            model = SGDDistributedModel(config=model_config)
            self.model = model


if __name__ == '__main__':
    with open("../example_configs/online_train.json", "r", encoding="utf8") as tc:
        train_conf = json.loads(tc.read())

    # print(train_conf)
    conductor = Conductor(train_config=train_conf)
    conductor.station = "charite"

    station_1_data = pd.read_csv("station_1_train.csv")
    station_2_data = pd.read_csv("station_2_df.csv")
    station_3_data = pd.read_csv("station_3_df.csv")

    conductor.set_data(station_3_data)
    conductor.run_train()
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(conductor.share_results())
