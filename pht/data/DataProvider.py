from abc import ABC, abstractmethod
from . import DataSource
from pandas.api.types import is_numeric_dtype
import pandas as pd
from sklearn.preprocessing import StandardScaler
import pickle
import json
import codecs


class DataProvider(ABC):
    @abstractmethod
    def discover(self):
        pass

    @abstractmethod
    def provide(self):
        pass


class TabularDataProvider(DataProvider):
    """
    Provider for tabular data based on a data source and an optional discovery config. Performs either data discovery
    or provides training data based on configuration options
    """

    def __init__(self, data_source: DataSource.DataSource, config: dict = None, config_path: str = None):
        self.data_source = data_source
        data_source.load()
        self.config = config

        if config_path and not config:
            with open(config_path, "r") as f:
                self.config = json.load(f)

        if not config_path:
            self.config_path = "data_config.json"
        else:
            self.config_path = config_path

    def discover(self, display=False, validate=True):
        """
        Perform data discovery for tabular data, updating a scaler object for continuous variables and collecting
        classes and class distributions for categorical variables, verify if all requested variables are present.

        Args:
            validate:
            display: boolean parameter controlling whether to display the discovery results, default = False

        Returns:

        """
        if validate:
            self.validate_data_source()

        cat_var_disc = self.discover_cat_vars()
        self.update_discovery_results(cat_var_disc)
        # TODO find target variable class distribution for classification tasks

        return self.config

    def provide(self):
        self.validate_data_source()
        training_data = self.create_training_data()
        return training_data

    def discover_cat_vars(self, display=False):
        """
        Find the categories and number of samples for each category present in the datasource for the selected
        categorical variables

        Args:
            display: print the results to stdout

        Returns:
            dictionary containing the categories and number of samples for each category

        """
        data: pd.DataFrame = self.data_source.data
        discovery_result = {}
        # Process categorical variables
        for var in self.config["variables"]["categorical"]:
            discovery_result[var] = data[var].value_counts().to_dict()

        # Target variable discovery

        # target = {"name": self.data_config["target"]}
        # # Process target variable get class distribution for classification tasks
        # if is_numeric_dtype(data[target["name"]]):
        #     if is_numeric_dtype(data[target["name"]]):
        #         if len(data[var].unique()) == 2:
        #             target["type"] = "binary"
        #         else:
        #             target["type"] = "regression"
        # else:
        #     if len(data[var].unique()) == 2:
        #         target["type"] = "binary"
        #     else:
        #         target["type"] = "multi-class"
        # if target["type"] in ["binary", "multiclass"]:
        #     class_counts = data[target["name"]].value_counts().to_dict()
        #     target["class_counts"] = class_counts
        #
        # discovery_result["target"] = target

        # Print the results of the data discover process
        if display:
            print(discovery_result)

        return discovery_result

    def discover_target_classes(self):
        # TODO implement this
        pass

    def update_discovery_results(self, cat_var_discovery: dict):
        """
        Update and merge the results of a previous discovery process, the standard scaler and categories and counts
        for the categorical variables

        Args:
            cat_var_discovery: results of performing cat var discovery

        Returns:

        """
        discovery_state = self.config["discovery"]["discovery_state"]
        # Update the total number of samples
        samples = len(self.data_source.data)
        discovery_state["total_samples"] = discovery_state.get("total_samples", 0) + samples

        # Process numeric variables using scikit-learn Standardscaler either update or create a new scaler object
        if discovery_state["scaler"]:
            scaler = self.load_scaler_()
        else:
            scaler = StandardScaler()
        scaler.partial_fit(self.data_source.data[self.config["variables"]["numeric"]])

        # Update categories and category counts
        for cat_var in self.config["variables"]["categorical"]:
            discovery_state["cat_var_discovery"][cat_var] = self._merge_cat_var_results(

                discovery_state["cat_var_discovery"].get(cat_var, None),
                cat_var_discovery.get(cat_var, None)
            )
        # Store cat_var discovery
        self.config["discovery"]["discovery_state"] = discovery_state

        # Store updated scaler
        self.save_scaler_(scaler)

        # Update the config file
        with open(self.config_path, "w") as f:
            json.dump(self.config, f, indent=2)



    @staticmethod
    def _merge_cat_var_results(res_1: dict, res_2: dict):
        """
        Merge two cat var discovery results by creating a union of the found categories and updating the number of
        sample for each existing category
        Args:
            res_1:
            res_2:

        Returns:

        """
        if not res_1:
            return res_2
        elif not res_2:
            return res_1
        else:
            merge_result = {}
            # Find the union of the found categories
            merged_categories = set(res_1.keys()).union(set(res_2.keys()))
            for cat in merged_categories:
                merge_result[cat] = res_1.get(cat, 0) + res_2.get(cat, 0)
        return merge_result

    def fill_missing_values(self, data):
        """
        Fill the missing values based on the results of the data discovery. Fills numerical variables with the calculated
        mean and categorical variables with the most frequent category

        Returns:
            pandas Dataframe with the missing values filled

        """
        discovery_state = self.config["discovery"]["discovery_state"]

        scaler: StandardScaler = self.load_scaler_()
        num_var_means = scaler.mean_
        # fill na with calculated means
        num_fill_values = {num_var: num_var_means[i] for i, num_var in enumerate(self.config["variables"]["numeric"])}
        data = data.fillna(num_fill_values)
        # Fill missing values in categorical columns with the most frequent category
        cat_fill_values = {}
        for cat_var in self.config["variables"]["categorical"]:
            cat_fill_values[cat_var] = max(discovery_state["cat_var_discovery"][cat_var],
                                           key=discovery_state["cat_var_discovery"][cat_var].get)

        data = data.fillna(cat_fill_values)
        return data

    def encode_categorical_variables(self, data):
        # TODO improve this
        cat_df = data[self.config["variables"]["categorical"]].copy()
        dummy_df = pd.DataFrame()
        discovery_state = self.config["discovery"]["discovery_state"]
        # Convert variable to categorical dtype and set the categories based on the discovery state
        for var in self.config["variables"]["categorical"]:
            categories = discovery_state["cat_var_discovery"][var].keys()
            cat_df[var] = cat_df[var].astype("category")
            cat_df[var].cat.set_categories(categories, inplace=True)
            dummies = pd.get_dummies(cat_df[var], prefix=var, dtype=float)
            dummy_df = pd.concat([dummy_df, dummies], axis=1)
        return dummy_df

    def create_training_data(self):

        # Get only the selected variables from the data source
        total_vars = self.config["variables"]["numeric"] + self.config["variables"]["categorical"]
        raw_data = self.data_source.data[total_vars]
        # get dataframe with one-hot encoded categorical variables
        imputed_data = self.fill_missing_values(raw_data)

        dummy_df = self.encode_categorical_variables(imputed_data)

        # Scale numerical variables with the standard scaler
        scaler = self.load_scaler_()
        num_df = imputed_data[self.config["variables"]["numeric"]].copy()
        # TODO impute missing values with the mean of the scaler object
        num_df = scaler.transform(num_df)
        num_df = pd.DataFrame(columns=self.config["variables"]["numeric"], data=num_df)
        X = pd.concat([num_df, dummy_df], axis=1)

        y = self.data_source.data[self.config["target"]]
        # Remove samples that do not containg the target variable
        X = X[y.notnull()]
        y = y[y.notnull()]
        return X, y

    def validate_data_source(self):
        """
        Validate the data source against the data config checking that all variables are present and that they have the
        correct types according to the config

        Returns:

        """

        cols = set(self.data_source.data.columns)
        # validate numeric variables
        for num_var in self.config["variables"]["numeric"]:
            if num_var in cols:
                # Throw error if the column in the data source is not a numeric dtype
                if not is_numeric_dtype(self.data_source.data[num_var]):
                    raise ValueError(f"Variable {num_var} should be a numeric dtype but is "
                                     f"{self.data_source.data[num_var].dtype}")
            else:
                raise ValueError(f"{num_var} not found in the data columns")
        # validate categorical columns
        for cat_var in self.config["variables"]["categorical"]:
            if cat_var in cols:
                if is_numeric_dtype(self.data_source.data[cat_var]):
                    raise ValueError(f"{cat_var} is not a supported categorical dtype")
            else:
                raise ValueError(f"{cat_var} not found in the data columns")

    def load_scaler_(self):
        """
        Load the scaler from a base64 encoded string contained in the data config file
        Returns:
            scikit-learn Standard Scaler
        """
        scaler_str = self.config["discovery"]["discovery_state"]["scaler"]
        scaler = pickle.loads(codecs.decode(scaler_str.encode(), "base64"))
        return scaler

    def save_scaler_(self, scaler):
        """
        Store the given scaler as a base64 encoded string in the config

        Args:
            scaler:

        Returns:

        """
        scaler_str = codecs.encode(pickle.dumps(scaler), "base64").decode()
        self.config["discovery"]["discovery_state"]["scaler"] = scaler_str

