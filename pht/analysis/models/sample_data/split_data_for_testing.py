import pandas as pd

df = pd.read_csv("houseprice_train.csv")
stat1 = df.iloc[:int(len(df)/3),:]
stat1.to_csv("station_1_houseprice.csv")
stat2 = df.iloc[int(len(df)/3): int(len(df)/2), :]
stat2.to_csv("station_2_houseprice.csv")
stat3 = df.iloc[int(len(df)/2):int(len(df)* 0.9), :]
stat3.to_csv("station_3_houseprice.csv")

test = df.iloc[int(len(df)* 0.9):, :]
test.to_csv("houseprice_test.csv")