from pht.analysis.models import DistributedModel
from pht.data import load_data
import json
from catboost import Pool, CatBoost, sum_models
import shap


class CatboostDistributedModel(DistributedModel):

    def __init__(self, name=None, data=None, target=None, config=None, model_id=None,
                 model_path=None, variables=None):
        super().__init__(
            name=name,
            data=data,
            target=target,
            config=config,
            model_id=model_id,
            model_path=model_path,
            variables=variables)
        # TODO make independent of data source

    def create_training_data_(self, shap_pool=False):
        """
        Create a catboost pool object with the given training data
        :return:
        :rtype:
        """
        training_data = self.data[self.variables].copy()
        target_data = self.data[self.target].copy()
        training_data = training_data[target_data.notnull()]
        target_data = target_data[target_data.notnull()]

        # if self.test_size:
        #     x_train, x_test, y_train, y_test = train_test_split(training_data, target_data, test_size=self.test_size)
        #     train_pool = Pool(x_train, y_train, cat_features=self.categorical_columns)
        #     test_pool = Pool(x_test, y_test, cat_features=self.categorical_columns)
        #     return train_pool, test_pool
        # else:
        cat_cols = self.find_categorical_variables(training_data)
        for col in training_data.columns:
            if training_data[col].dtype.name == "string":
                training_data[col] = training_data[col].astype("object")
            if col in cat_cols:
                training_data[col] = training_data[col].fillna("Unknown")
            if col not in cat_cols:
                training_data[col] = training_data[col].astype(float)
        # TODO make this more general
        if self.config["loss_function"] == "RMSE":
            target_data = target_data.astype(float)
        train_pool = Pool(training_data, target_data, cat_features=cat_cols)
        if shap_pool:
            return training_data

        return train_pool

    def setup_with_config(self, config):
        """
        Create a catboost model based on the given config
        :param config:
        :type config:
        :return:
        :rtype:
        """
        predictor = CatBoost(dict(
            loss_function=config["loss_function"],
            iterations=config.get("iterations", 50),
            learning_rate=config.get("learning_rate", 0.03),
            )
        )
        self.predictor = predictor

    def fit(self, save_model=True, display_results=True):
        if not self.predictor:
            self.setup_with_config()
        train_data = self.create_training_data_()
        self.predictor.fit(train_data)
        if display_results:
            shap_pool = self.create_training_data_(shap_pool=True)
            explainer = shap.TreeExplainer(self.predictor)
            shap_values = explainer.shap_values(shap_pool)
            shap.summary_plot(shap_values, shap_pool)
            shap.force_plot(explainer.expected_value, shap_values[0,:], shap_pool.iloc[0,:], matplotlib=True)
        # TODO store/display results of training

    def export_results(self):
        # TODO implement this and store results in db
        pass

    def predict(self, data):
        """
        Use the trained model to generate predictions based on the given data
        :param data:
        :type data:
        :return:
        :rtype:
        """
        prediction = self.predictor.predict(data)
        return prediction

    def load(self):
        """
        Load a saved model from its json representation and set it as the predictor
        :param filepath:
        :type filepath:
        :return:
        :rtype:
        """
        self.predictor = CatBoost().load_model(self.model_path, format="json")

    def load_model(self, path):
        """
        Load any catboost model from a json file
        :param path: path to the json model
        :type path:
        :return:
        :rtype:
        """
        return CatBoost().load_model(path, format="json")

    def distribute(self):
        """
        Exports the model or its json representation for the conductor to distribute to different locations
        :return:
        :rtype:
        """
        # TODO read the saved model and return the json object representing the model

        pass

    def assemble(self, models):
        """
        Combine multiple models into one and set it as the predictor for this model
        :param models:
        :type models:
        :return:
        :rtype:
        """
        print(models)
        catboost_models = []
        for model in models:
            path = f"model_{model['station']}.json"

            with open(path, "w") as mf:
                mf.write(model["model"])
            catboost_models.append(path)
        print(catboost_models)
        self.predictor = sum_models(models)

    def save(self):
        """
        Store the catboost model as a json object at the given file path
        :return:
        :rtype:
        """
        self.predictor.save_model(self.model_path, format="json")

    @staticmethod
    def find_categorical_variables(data):
        """
        Finds categorical variables in the data and returns a list of their names
        :param data:
        :type data:
        :return:
        :rtype:
        """
        cat_vars = []
        for col in data.columns:
            if data[col].dtype.name in ["object", "string", "category"]:
                cat_vars.append(col)
        return cat_vars


if __name__ == '__main__':
    df_sars = load_data(
        "C:\\hypothesis\\repositories\\server\\walzLabBackend\\notebook\\15052020SARS-CoV-2_final.xlsx",
        two_sheets=False)

    # Split into three data sets
    station_1_df = df_sars.sample(frac=0.33)
    station_2_df = df_sars.sample(frac=0.33)
    station_3_df = df_sars.sample(frac=0.33)
    station_1_df.to_csv("station_1_df.csv", index_label=False)
    station_2_df.to_csv("station_2_df.csv", index_label=False)
    station_3_df.to_csv("station_3_df.csv", index_label=False)

    with open("../../example_configs/test_train_config.json", "r", encoding="utf8") as tc:
        train_conf = json.loads(tc.read())
    model_conf = train_conf["model"]
    target = train_conf["target"]
    print(target)
    vars = train_conf["variables"]
    cm = CatboostDistributedModel(name="test",
                                  data=df_sars,
                                  target=target,
                                  variables=vars)
    pool = cm.create_training_data_()
    cm.set_config(model_conf)
    cm.fit()
