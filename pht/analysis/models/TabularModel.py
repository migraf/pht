from .DistributedModel import DistributedModel
from typing import List
from fastai.tabular.all import *


class TabularModel(DistributedModel):
    """
    Model for working with tabular data
    """

    def __init__(self, config: dict = None, data: pd.DataFrame = None, target: pd.Series = None,
                 prediction_type: str = None, layers: List[int] = None):
        if config:
            super().__init__(config)
            config["model"]

        else:
            self.X = data
            self.y = target
            self.prediction_type = prediction_type


    def save(self, path):
        pass

    def load(self, path):
        pass

    def setup_with_config(self, predictor_config: dict, data_config: dict):
        pass

    def create_training_data(self, path, target):
        pass

    def fit(self, X, y):
        pass

    def predict(self, data):
        pass

    def distribute(self):
        pass

    def update(self):
        pass
