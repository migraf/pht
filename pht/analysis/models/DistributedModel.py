import numpy as np
import os
import json
from abc import ABC, abstractmethod
import pandas as pd


class DistributedModel(ABC):
    """
    Base class for a distributed model using tabular data
    """
    def __init__(self, config: dict = None):
        self.config = config

    @abstractmethod
    def save(self, path):
        pass

    @abstractmethod
    def load(self, path):
        pass

    @abstractmethod
    def setup_with_config(self, predictor_config: dict, data_config: dict):
        pass

    @abstractmethod
    def fit(self, X, y):
        pass

    @abstractmethod
    def predict(self, data):
        pass

    @abstractmethod
    def distribute(self):
        pass

    def set_config(self, config):
        self.config = config

    @abstractmethod
    def update(self, dist_result):
        pass

    def serialize(self):
        pass

